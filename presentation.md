---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #1a1a1a
color: #fff
---

# **Group 3**
### Webserver Migration

---

## **Current context:**

- Servers configuration: 
    - Hosted by a French company, remotely managed by ReQuest.
    - Debian 11 OS stable.
- Website:
    - WordPress based
    - 1 million French yearly visitors.
    - No online orders/payments.
- Database
    - Relational MySQL DB

---
## **Current configuration:**
---
## **New needs:**
- ReQuest is extending in Europe meaning:
    - More visitors (20 Millions per year)
    - Some visitors may get high latency from their location
    - Website must adapt to different languages based on location.

- The company will sell their stories online from its website:
    - Website needs to support:
        - User accounts
        - Orders and payments

---

#### **First Solution Proposal:**
###### *The Lift and Shift Migration*

---
#### **First Solution Proposal:**
###### *The Lift and Shift Migration*  
.
##### Benefits of the solution:
- Fast migration
- No need to replatform or refactor 
- Low initial cost
- Short adapatation period

---
#### **Second Solution Proposal:**
###### *The Server-based solution*
---
#### **Second Solution Proposal:**
###### *The Server-based solution*
.
##### Benefits of the solution:
- Benefiting from the cloud without losing control on servers
- No need to refactor the app
- Scalability
- High availabilty
- Cost optimized (compared to the previous solution)

---
#### **Third Solution Proposal:**
###### *The serverless solution*

---

#### **Third Solution Proposal:**
###### *The serverless solution*

.
##### Benefits of the solution:
- Fully benefiting from the cloud advantages
- No need to worry about servers maintenance
- High scalability
- High availabilty
- Cost effective
- Pay as you go

---
### **Cost comparison:**

| Solution    | On premise  | Lift & Shift | Server-based |Serverless |
| ----------- | ----------- |--------------|-|-|
|Annual Cost  | $$$         | $462.302,28* | | |
|Need admins  | Yes         | Yes          | Yes | No |

**Cost for one year based on a 3 year all-upfront  saving plan*

---

![bg left:40% 80%](https://marp.app/assets/marp.svg)

# **Marp**

Markdown Presentation Ecosystem

https://marp.app/

---

# How to write slides

Split pages by horizontal ruler (`---`). It's very simple! :satisfied:

```markdown
# Slide 1

foobar

---

# Slide 2

foobar
```