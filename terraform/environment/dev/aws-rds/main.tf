terraform {
  required_version = "> 0.15.0"
  backend "local" {
    path = "./terraform.tfstate"
  }
}

data "terraform_remote_state" "aws_vpc" {
  backend = "local"
  config = {
    path = "../aws-vpc/terraform.tfstate"
  }
}

locals {
  vpc_id    = data.terraform_remote_state.aws_vpc.outputs.vpc_id
  subnet_id = data.terraform_remote_state.aws_vpc.outputs.subnet_id
  sg_id     = data.terraform_remote_state.aws_vpc.outputs.sg_id
}

module "aws_rds" {
  source                  = "../../../modules/aws-rds"
  vpc_id                  = local.vpc_id
  region                  = "eu-west-3"
  availability_zone       = "eu-west-3a"
  allowed_security_groups = [local.sg_id]
  db_port                 = 3306
  db_name                 = "wordpress"
  db_allocated_storage    = 5
  db_instance_class       = "db.t2.micro"
  db_storage_type         = "gp2"
  db_username             = "wordpress"
  db_engine               = "mysql"
  db_engine_version       = "5.7"
  db_parameter_group_name = "default.mysql5.7"
  db_skip_final_snapshot  = true
  tags                    = {
    environment = "dev"
    project     = "ecs-wordpress"
    terraform   = true
  }
}

output "db_endpoint" {
  value = module.aws_rds.db_endpoint
}

output "db_address" {
  value = module.aws_rds.db_address
}

output "db_port" {
  value = module.aws_rds.db_port
}

output "db_username" {
  value = module.aws_rds.db_username
}

output "db_password" {
  value     = module.aws_rds.db_password
  sensitive = true
}
