terraform {
  required_version = "> 0.15.0"
  backend "local" {
    path = "./terraform.tfstate"
  }
}

module "aws_vpc" {
  source            = "../../../modules/aws-vpc"
  region            = "eu-west-3"
  availability_zone = "eu-west-3a"
  tags              = {
	environment = "dev"
	project     = "ecs-wordpress"
	terraform   = true
  }
}

output "vpc_id" {
  value = module.aws_vpc.vpc_id
}

output "availability_zone" {
  value = module.aws_vpc.availability_zone
}

output "subnet_id" {
  value = module.aws_vpc.subnet_id
}

output "sg_id" {
  value = module.aws_vpc.sg_id
}